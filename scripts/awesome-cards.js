'use strict';

function Card(config) {
	this._config = config;
	
	this.id = null;
	this.LS = localStorage;
	this.cardsData = this.LS.getItem('cards');

	this.parseCardsData = JSON.parse(this.cardsData);

	this._cardTitle = '';
	this._cardText = '';
	this._cardImportant = false;
	this._cardBlock = null;
	this._titleEl = null;
	this._textEl = null;
	this._importantEl = null;
	this._deleteButtonEl = null;
	this._cardsBlock = null;
	this._changeTitleEl = null;
	this._changeTextEl = null;
	this._changeButtonEl = null;
	this._buttonContainer = null;
	this._containerCard = null;
};

Object.assign(Card.prototype, {
	
	_createElements: function () {
		this._containerCard = document.createElement('div');
		this._cardBlock = document.createElement('div');
		this._titleEl = document.createElement('h2');
		this._importantEl = document.createElement('input');
		this._textEl = document.createElement('p');
		this._deleteButtonEl = document.createElement('button');
		this._changeButtonEl = document.createElement('button');
		this._buttonContainer = document.createElement('div');
		this._changeTitleEl = document.createElement('input');
		this._changeTextEl = document.createElement('input');
	},

	_addElements: function () {
		this._cardsBlock.appendChild(this._containerCard);

		this._containerCard.appendChild(this._cardBlock);
		this._containerCard.appendChild(this._buttonContainer);
		

		this._cardBlock.appendChild(this._titleEl);
		this._cardBlock.appendChild(this._importantEl);
		this._cardBlock.appendChild(this._changeTitleEl);
		this._cardBlock.appendChild(this._textEl);
		this._cardBlock.appendChild(this._changeTextEl);
		
		
		this._buttonContainer.appendChild(this._changeButtonEl);
		this._buttonContainer.appendChild(this._deleteButtonEl);

		
		
	},

	_addElementsContent: function () {
		this._containerCard.classList.add('container-card');
		this._cardBlock.classList.add('card-body');
		this._titleEl.textContent = this._cardTitle;
		this._titleEl.classList.add('card-title');
		this._textEl.textContent = this._cardText;
		this._textEl.classList.add('card-text');
		this._buttonContainer.classList.add('buttonContainer');
		this._deleteButtonEl.textContent = 'DELETE';
		this._deleteButtonEl.classList.add('btn-outline-danger');
		this._deleteButtonEl.classList.add('btn');
		this._changeButtonEl.textContent = 'CHANGE';
		this._changeButtonEl.setAttribute('data-action', 'change');
		this._changeButtonEl.classList.add('btn');
		this._changeButtonEl.classList.add('btn-outline-success');
		this._importantEl.setAttribute('type', 'checkbox');
		this._importantEl.classList.add('important-block')
		
		if(this._cardImportant) {
			this._importantEl.setAttribute('checked', 'checked');
			this._importantEl.setAttribute('disabled', 'disabled');
		} else {
			this._importantEl.setAttribute('disabled', 'disabled');
		}
		
		this._changeTitleEl.style.display = 'none';
		this._changeTextEl.style.display = 'none';
	},

	_generateCard: function () {
		this._createElements();
		this._addElementsContent();
		this._addElements();
		this._saveState();
	},

	_saveState: function () {
		if(!this._config.id) {
			this._generateId();

			if(this.cardsData !== null) {
			
				this._updateState();
			} else {
				this.parseCardsData = {};
				this._updateState();
			}
			
		} else {
			this.id = this._config.id;
		}
	},

	_updateState: function () {
		this.parseCardsData[this.id] = {
			title: this._cardTitle,
			text: this._cardText,
			important:this._cardImportant
		};
		this._importantEl.setAttribute('disabled', 'disabled');
		self.cardsData = self.LS.getItem('cards');

		var strCardsData = JSON.stringify(this.parseCardsData);

		this.LS.setItem('cards', strCardsData);
	},

	_getData: function () {
		this._cardTitle = this._config.title;
		this._cardText = this._config.text;
		this._cardImportant = this._config.important;
		
	},

	_changeCard: function () {
		this._titleEl.style.display = 'none';
		this._textEl.style.display = 'none';

		this._changeTitleEl.style.display = 'block';
		this._changeTextEl.style.display = 'block';

		
		this._changeTitleEl.value = this._cardTitle;
		this._changeTextEl.value = this._cardText;
		this._importantEl.checked = this._cardImportant;

		this._changeButtonEl.textContent = 'SAVE';
		this._changeButtonEl.setAttribute('data-action', 'save');
	},

	_saveCard: function () {
		this._titleEl.textContent = this._changeTitleEl.value;
		this._textEl.textContent = this._changeTextEl.value;

		this._cardTitle = this._changeTitleEl.value;
		this._cardText = this._changeTextEl.value;

		this._cardImportant = this._importantEl.checked;

		this._changeTitleEl.style.display = 'none';
		this._changeTextEl.style.display = 'none';

		this._titleEl.style.display = 'block';
		this._textEl.style.display = 'block';

		this._changeButtonEl.textContent = 'CHANGE';
		this._changeButtonEl.setAttribute('data-action', 'change');

		this._updateState();
	},

	_attachEvents: function () {
		var self = this;
		this._deleteButtonEl.addEventListener('click', function (event) {
			event.preventDefault();

			self._cardsBlock.removeChild(self._containerCard);
			self.cardsData = self.LS.getItem('cards');

			self.parseCardsData = JSON.parse(self.cardsData);
			delete self.parseCardsData[self.id];
			
			var strCardsData = JSON.stringify(self.parseCardsData);
			self.LS.setItem('cards', strCardsData);
			
		});

		this._changeButtonEl.addEventListener('click', function (event) {
			event.preventDefault();
			var action = self._changeButtonEl.getAttribute('data-action');
			self._importantEl.removeAttribute('disabled');
			switch (action) {
				case 'change':
					self._changeCard();
					break;
				case 'save':
					self.parseCardsData = JSON.parse(self.LS.getItem('cards'));
					self._saveCard();
					break;
			}
		});
	},

	_generateId: function () {
		this.id = new Date().getTime();
	},

	init: function () {
		this._cardsBlock = document.querySelector('#cardsBlock');

		this._getData();
		this._generateCard();
		this._attachEvents();
	},
});

