'use strict';
var LS = localStorage;

var cards = LS.getItem('cards');

if(cards !== null) {
	var parseCards = JSON.parse(cards);
	for(var key in parseCards) {
		var cardObj = parseCards[key];
		cardObj.id = key;
	
		var card = new Card(cardObj);
		card.init();
	}
}


var createButton = document.querySelector('#createButton');
createButton.addEventListener('click', function (event) {
	event.preventDefault();
	var createForm = document.querySelector('#createForm');

	var cardObj = {};

	var titleField = createForm.querySelector('#title');
	var textField = createForm.querySelector('#text');
	var importantField = createForm.querySelector('#important');

	cardObj.title = titleField.value;
	cardObj.text = textField.value;
	cardObj.important = importantField.checked;

	titleField.value = '';
	textField.value = '';
	importantField.checked = false;

	var card = new Card(cardObj);
	card.init();
});
